
enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace,
};

enum Suit
{
	Spades,
	Hearts,
	Clubs,
	Diamonds,
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	return 0;
}
